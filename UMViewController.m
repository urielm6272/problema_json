//
//  UMViewController.m
//  tarea_json
//
//  Created by Uriel Andrade on 1/21/14.
//  Copyright (c) 2014 Uriel Andrade. All rights reserved.
//

#import "UMViewController.h"
#import "UMDetailViewController.h"

@interface UMViewController ()

@property (nonatomic, strong) NSArray *venues;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSIndexPath *lastSelectedIndexPath;

@end

@implementation UMViewController

#pragma mark - Private Selectors
- (void) customizeViewInterface {
    self.title = @"Venues";
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self
                       action:@selector(refreshInfo:)
             forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
}

-(void)refreshInfo:(UIRefreshControl *)sender {
    self.title = @"Venues";
    NSURL *url = [NSURL URLWithString:@"https://api.foursquare.com/v2/venues/search?ll=19.41020,-99.17758&radius=800&locale=es&v=20140121&client_secret=OV3JVHFAU32WALYUZ1PJOHUDU2PXP5E5CUEQC2QJC5JAKWPO&client_id=QZVLKFPQ5MNXF3ISLUNYVBTEENWKEMZ3FZMEOKEAIYW1FWLS&categoryId=4bf58dd8d48988d18e941735"];
    NSData *venueData = [NSData dataWithContentsOfURL:url];
    NSError *error;
    NSDictionary *foursquareInfo = [NSJSONSerialization JSONObjectWithData:venueData options:kNilOptions error:&error];
    
    if (foursquareInfo){
        self.venues = [foursquareInfo valueForKeyPath:@"response.venues"];
        [self.tableView reloadData];
        [sender endRefreshing];
    }
    else {
        NSLog(@"Error %@", [error localizedDescription]);
        [sender endRefreshing];
    }
}

//-(void) viewWillAppear:(BOOL)animated {
  //  [super viewWillAppear:<#animated#>];
//}

#pragma mark - UITableViewDataSource Selectors
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.venues.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    NSDictionary *venueInfo = self.venues[indexPath.row];
    
    cell.textLabel.text = venueInfo[@"name"];
    cell.detailTextLabel.text = [venueInfo valueForKeyPath:@"location.address"];
    
    return cell;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self customizeViewInterface];
}

#pragma mark - UITableViewDelegate Selectors
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
 //   NSString *venueIdentifier = self.venues[indexPath.row][@"id"];
    //NSLOG
    self.lastSelectedIndexPath = indexPath;
    UMDetailViewController *detailController = [[UMDetailViewController alloc] initWithNibName:@"UMDetailViewController" bundle:nil];
    [self.navigationController pushViewController:detailController animated:YES];
}

@end
