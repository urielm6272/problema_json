//
//  UMAppDelegate.h
//  tarea_json
//
//  Created by Uriel Andrade on 1/21/14.
//  Copyright (c) 2014 Uriel Andrade. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
