//
//  UMDetailViewController.m
//  tarea_json
//
//  Created by dcl14 on 22/01/14.
//  Copyright (c) 2014 Uriel Andrade. All rights reserved.
//

#import "UMDetailViewController.h"

@interface UMDetailViewController ()

@end

@implementation UMDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
