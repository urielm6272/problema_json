//
//  main.m
//  tarea_json
//
//  Created by Uriel Andrade on 1/21/14.
//  Copyright (c) 2014 Uriel Andrade. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([UMAppDelegate class]));
    }
}
